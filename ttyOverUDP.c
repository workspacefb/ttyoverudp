/*******************************************************************************
 Module Serial/UDP
 *//*!

 \brief  Simulation liaison uart par protocole UDP sur IP
 *******************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "ttyOverUDP.h"

// 2 sockets are used: 
// - 1st to read on it
// - 2nd to write on it
//
#define PORT_READ    10000
#define PORT_WRITE   10001

// Global variables
int g_sockfds[2] = {0};
fd_set g_rset;

// Returns 1 in case of success !
//
u8 TtyOverUdpOpen(u8 u8Channel)
{
  // Creating socket file descriptors
  // 1st socket: read on it
  if ( (g_sockfds[0] = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0)) < 0 ) {
    perror("socket creation failed");
    return -1;
  }

  // bind on channel 0 (localhot, port 0)
  struct sockaddr_in servaddr = {0};
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT_READ);
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  if ( bind(g_sockfds[0], (const struct sockaddr*)&servaddr, sizeof(servaddr)) < 0 ) {
    perror("bind failed");
    return -1;
  }

/*  struct timeval receive_timeout = {1, 0};
  if ( setsockopt(g_sockfds[0], SOL_SOCKET, SO_RCVTIMEO, &receive_timeout, sizeof(receive_timeout)) != 0)
  {
    perror("set SO_RCVTIMEO failed");
  }
*/
  // 2nd socket: write on it
  if ( (g_sockfds[1] = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0)) < 0 ) {
    perror("socket creation failed");
    return -1;
  }

  return 1;
}

u8 TtyOverUdpClose (u8 u8Channel)
{
  if (g_sockfds[0] > 0)
    close(g_sockfds[0]);
  if (g_sockfds[1] > 0)
    close(g_sockfds[1]);
  return 0;
}

u8 TtyOverUdpWriteDirect ( u8 u8TtyChannel, u8 u8Data )
{
  fprintf(stderr, "%s to be implemented !\n", __FUNCTION__);
  return 0;
}

// Write buffer on 2nd socket
u16 TtyOverUdpWriteDirectArray ( u8 u8Ch , u8* pu8Data , u16 u16Len)
{
  struct sockaddr_in servaddr = {0};
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT_WRITE);
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  return sendto(g_sockfds[1], (const char *)pu8Data, u16Len, MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr));
}

u8 TtyOverUdpReadDirect (u8 u8Ch , u8* pu8Data)
{
  fprintf(stderr, "%s to be implemented !\n", __FUNCTION__);
  return 0;
}

// Read buffer on 1st socket
u16 TtyOverUdpReadDirectArray(u8 u8Ch, u8* pu8Data, u16 u16MaxLen)
{
static u8 nIndexBuffer = 0;
#define MAX_FRAMELEN 100
u8 tmpBuff[MAX_FRAMELEN] = {0};

  // If previous read was to get the length of the frame
  // then read the buffer shifted by 1 byte (to not have the frame length twice)
  if ((0 == nIndexBuffer) && (u16MaxLen > 1))
    nIndexBuffer = 1;
  
  if (1 == u16MaxLen)
    nIndexBuffer = 0;
  
  bzero(pu8Data, u16MaxLen*sizeof(*pu8Data));

  //MSG_PEEK flag MUST be used when reading first byte (to get the length of the frame)
  // and keep data for the 2nd read
  int n = recvfrom(g_sockfds[0], (char *)tmpBuff, u16MaxLen+nIndexBuffer, (u16MaxLen==1)?MSG_PEEK:0, (struct sockaddr*)NULL, NULL);
  if (n > 0)
  {
    fprintf(stdout, "[%s] Nb byte read: %d \n", __FUNCTION__, n);
    memcpy(pu8Data, &tmpBuff[nIndexBuffer], n-nIndexBuffer);

    return n-nIndexBuffer;
  }

  return 0;
}

u8 TtyOverUdpTxEmpty  ( u8 u8Ch )
{
  fprintf(stderr, "%s to be implemented !\n", __FUNCTION__);
  return 0;
}

// Wait on available data received
// return >0 in case of success
// 
u8 TtyOverUdpRxAvail  ( u8 u8Ch )
{
  // clear the descriptor set
  FD_ZERO(&g_rset);

  // set sockfd in readset
  FD_SET(g_sockfds[0], &g_rset);

  int numfd = g_sockfds[0] + 1;

  // select the ready descriptor
  int nready = select(numfd, &g_rset, NULL, NULL, NULL);
  if (-1 == nready) {
    perror("select"); // error occurred in select()
  }
  else if (0 == nready) {
    printf("Timeout occurred !\n");
  }
  else {
    // if udp socket is readable receive the message.
    if (FD_ISSET(g_sockfds[0], &g_rset)) {
      FD_CLR(g_sockfds[0], &g_rset); //clear the set
      return 1;
    }
  }
  return nready;
}

void TtyOverUdpFlush  ( u8 u8Ch )
{
  fprintf(stderr, "%s to be implemented !\n", __FUNCTION__);
}

void TtyOverUdpInitGen ( u8 u8TtyChannel )
{
//Nothing to do
}

void TtyOverUdpEnterCrit ( u8 u8Ch )
{
  fprintf(stderr, "%s to be implemented !\n", __FUNCTION__);
}

void TtyOverUdpLeaveCrit ( u8 u8Ch )
{
  fprintf(stderr, "%s to be implemented !\n", __FUNCTION__);
}
