#!/usr/bin/env python

import socket
import time

localIP      = "127.0.0.1"
Channel0Port = 10000
Channel1Port = Channel0Port + 1
bufferSize   = 1024

FRAME_SERVICE_TYPE = 3;

# List of the different types of service
NETWORK_TOPOGRAPHY = 10;
ALARM_ID = 12;
DOMESTIC_ID = 13;
COMMODE_DEVICE = 14;
SERVICE_17 = 17;
X3D_STACK_VERSION = 200;
KEY_SOFTWARE_VERSION = 201;
CAPABILITIES = 203;

def analyzeFrame(frame):
#    frame_converted = frame.encode('hex')
    serviceNumberHex = frame[FRAME_SERVICE_TYPE].encode('hex')
    serviceNumber = int(serviceNumberHex, 16)
    response = ''

    if (serviceNumber == NETWORK_TOPOGRAPHY):
        response = '\x23\x06\x80\x0A\x00\x00\x00\x00\x19\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xFF\x34'
    elif (serviceNumber == ALARM_ID):
        response = '\x0E\x06\x80\x0C\x00\x00\x00\x00\x04\x00\x00\x00\xFF\x5C'
    elif (serviceNumber == DOMESTIC_ID):
        response = '\x0E\x06\x80\x0D\x00\x00\x00\x00\x04\x00\x00\x00\xFF\x5B'
    elif (serviceNumber == COMMODE_DEVICE):
        response = '\x14\x06\x80\x0E\x00\x00\x00\x00\x0A\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xF6\x57'
    elif (serviceNumber == X3D_STACK_VERSION):
        response = '\x11\x06\x80\xC8\x00\x00\x00\x00\x07\x00\x00\x00\x00\x00\x00\xFE\x9A'
    elif (serviceNumber == KEY_SOFTWARE_VERSION):
        response = '\x0E\x06\x80\xC9\x00\x00\x00\x00\x04\x00\x00\x00\xFE\x9F'
    elif (serviceNumber == CAPABILITIES):
        response = '\x0F\x06\x80\xCB\x00\x00\x00\x00\x05\x0E\x00\x00\x00\xFE\x8D'
    elif (serviceNumber == SERVICE_17):
        response = '\x04\x06\xFF\xF6'
    else:
        print("No service type for this value : %d\n")%serviceNumber

    return response


# Create datagram sockets
UDPChannel0Socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPChannel1Socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Bind to channel 1 address and ip
UDPChannel1Socket.bind((localIP, Channel1Port))

print("UDP device simulator up and listening")

# Listen for incoming datagrams on channel 1
while(True):
    bytesAddressPair = UDPChannel1Socket.recvfrom(bufferSize)
    message = bytesAddressPair[0]
    address = bytesAddressPair[1]
    #clientMsg = "<------- {}%s".format(message)
    clientIP  = "Client IP Address:{}".format(address)
    print("<------- %s")%message.encode('hex')
    # print(clientIP)


    bytesToSend = analyzeFrame(message)
    bytesToSend_hex = bytesToSend.encode('hex')
    print("-------> %s\n")%bytesToSend_hex

    # Sending a reply to client on channel 0
    UDPChannel0Socket.sendto(bytesToSend, (localIP, Channel0Port))


