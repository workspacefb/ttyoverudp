#ifndef _TESTSUITE_TTYOVERUDP_H_
#define _TESTSUITE_TTYOVERUDP_H_

/**
 * Declaration of test function (implemented in test2.c)
 */
TEST(test1);
TEST(test2);

/**
 * Definition of all tests in this test suite
 */
TEST_SUITE(T1) {
   TEST_ADD(test1),
   TEST_ADD(test2),
   TEST_SUITE_CLOSURE
};


#endif //_TESTSUITE_TTYOVERUDP_H_
