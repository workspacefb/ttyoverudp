#include <stdio.h>
#include "cu.h"
#include "testSuite_TTYOverUdp.h"

#define maxi(x,y) (x>y?x:y)

TEST(test1) {
  assertTrue(maxi(0,2) == 1);
}

TEST(test2) {
   assertFalse(0);
   assertNotEquals(1, 2);
   fprintf(stderr, "Hello from test2\n");
}

TEST_SUITES{
    TEST_SUITE_ADD(T1),
    TEST_SUITES_CLOSURE
};

int main(int argc, char *argv[])
{
   // Set up directory where are stored files with outputs from test suites
   //
   // Attention: le repertoire doit être créé avant execution !!
   CU_SET_OUT_PREFIX("regressions/"); 

   // Run all test suites
   CU_RUN(argc, argv);

   return 0;
}
