# **Introduction**

Le but de cette solution est de pouvoir effectuer des tests automatiques et notamment de régression au plus tôt purement logiciel.

L'idée est d'utiliser un canal de communication IP entre le SDK_tydom et le simulateur de device.

La solution fonctionnera sur Linux pour plus de souplesse. 



# Interface type liaison série

Afin de rester au plus près des protocole et interfaces de communications existantes, la solution est de garder la même interface que celle utilisée avec une clef radio USB. Et donc avoir des fonctions tty* qui interragissent avec une socket.



## Interface actuelle de communication Linux avec clef USB radio

Actuellement SDK_tydom fonctionne sous Linux et communique en X3D avec une clef USB comme indiqué sur le schéma ci-dessous:

![](Tydomux.ttyUSB.drawio.png)



## Nouvelle interface type liaison série sur IP (UDP) pour simulation 

La nouvelle interface peut être représentée comme suit:

![Tydomux.SerialUDP.drawio](Tydomux.SerialUDP.drawio.png)



## Tests avec simulateur python 

SDK_tydom sous Linux avec simulation de la radio par le protocole UDP peut être en partie testé.
Pour cela, récupérer le projet ttyoverudp sous:

<code>
git@bitbucket.org:workspacefb/ttyoverudp.git 
</code>

Les tests se font dans 2 terminaux.
Dans le 1er terminal, executer le simulateur Python avec la version python 2.7:
<code>
pdb2.7 simulatorDevice.py
</code>

Dans le 2eme terminal (celui du sdk_tydom ttyoverudp), effacer et executer le programme comme suit:
- remove old eeprom file: 
<code>
sudo rm /var/backups/deltadore/64bits_dd_tydom_eeprom
</code>

- Execution:
<code>
sudo output/P21809110_LINUX/D2180_TYDOMUX_1_UDP -m 001A25FFFF1?
</code>
Remplacer ? par le digit qui vous a été assigné