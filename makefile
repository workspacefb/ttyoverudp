all: testSuite clientUDP

.PHONY: clean

clean:
	rm -rf $(ODIR)

IDIR =.
CC=gcc
CFLAGS=-I$(IDIR) -g -O0

ODIR=obj

LIBS=-lm

_DEPS = ttyOverUDP.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = clientUDP.o ttyOverUDP.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: %.c $(DEPS)
	@mkdir -p $(dir $@)
	$(CC) -c -o $@ $< $(CFLAGS)

clientUDP: $(OBJ)
	$(CC) -o $(ODIR)/$@ $^ $(CFLAGS) $(LIBS)

#Unit test
$(ODIR)/testsuite-%.o: UnitTest/%.c UnitTest/%.h $(DEPS)
	@echo "compiling unit test.."	
	@echo "TESTUNIT_CFLAGS= "	$(TESTUNIT_CFLAGS)
	@mkdir -p $(dir $@)
	$(CC) -c -o $@ $< $(CFLAGS) $(TESTUNIT_CFLAGS) $(TESTUNIT_LIBS)

$(ODIR)/cu.o: UnitTest/cu/cu.c UnitTest/cu/cu.h $(DEPS)
	@echo "compiling CU .."	
	@mkdir -p $(dir $@)
	$(CC) -c -o $@ $< $(CFLAGS) $(TESTUNIT_CFLAGS)

$(ODIR)/libcu.a: $(ODIR)/cu.o
	ar cr $@ $^
	ranlib $@

CUDIR=UnitTest/cu

TESTUNIT_CFLAGS=$(CFLAGS) -I$(CUDIR)
TESTUNIT_LIBS = $(LIBS) -L$(ODIR) -lcu

_TESTSUITE_OBJS = testSuite_TTYOverUdp.o
TESTSUITE_OBJS = $(patsubst %,obj/testsuite-%,$(_TESTSUITE_OBJS))

testSuite: $(ODIR)/libcu.a $(TESTSUITE_OBJS)
	$(CC) -o $(ODIR)/$@ $(TESTSUITE_OBJS) $(TESTUNIT_CFLAGS) $(TESTUNIT_LIBS)


#Unit Test example
obj/cutest-%.o: UnitTest/cu/testsuites/%.c
	@echo "compiling unit test .."
	@echo "TESTUNIT_CFLAGS= "	$(TESTUNIT_CFLAGS)
	@mkdir -p $(dir $@)
	$(CC) -c -o $@ $< $(CFLAGS) $(TESTUNIT_CFLAGS) $(TESTUNIT_LIBS)

_EXAMPLE_OBJS = test.o test2.o
EXAMPLE_OBJS = $(patsubst %,obj/cutest-%,$(_EXAMPLE_OBJS))

example: $(ODIR)/libcu.a $(EXAMPLE_OBJS)
	$(CC) $(CFLAGS) -o $@ $(EXAMPLE_OBJS) $(TESTUNIT_CFLAGS) $(TESTUNIT_LIBS)

