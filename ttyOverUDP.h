/*******************************************************************************
  Module Serial/UDP
*/ /*!
  \brief  Simuler UART par protocole UDP
*******************************************************************************/

#ifndef _UART_H_
#define _UART_H_

#if __has_include("DDDrvPortRtos.h")
#include "DDDrvPortRtos.h"
#endif
#define RX_OPEN     0x01
#define RX_CLOSED   0
#define TX_OPEN     0x02
#define TX_CLOSED   0

enum {
TTY_USB0=0,
TTY_X3DV0,
/* ... */
TTY_CHNB
};

u8 TtyOverUdpOpen(u8 u8Channel);
u8 TtyOverUdpClose (u8 u8Channel);
u8 TtyOverUdpWriteDirect ( u8 u8TtyChannel, u8 u8Data );
u16 TtyOverUdpWriteDirectArray ( u8 u8Ch , u8* pu8Data , u16 u16Len);
u8 TtyOverUdpReadDirect (u8 u8Ch , u8* pu8Data);
u16 TtyOverUdpReadDirectArray(u8 u8Ch, u8* pu8Data, u16 u16MaxLen);
void TtyOverUdpEnterCrit ( u8 u8Ch );
void TtyOverUdpLeaveCrit ( u8 u8Ch );
u8 TtyOverUdpTxEmpty  ( u8 u8Ch );
u8 TtyOverUdpRxAvail  ( u8 u8Ch );
void TtyOverUdpFlush  ( u8 u8Ch );
void TtyOverUdpInitGen ( u8 u8TtyChannel );

#endif // _UART_H_
