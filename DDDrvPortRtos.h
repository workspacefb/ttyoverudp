#ifndef _DDDRVPORTRTOS_H_
#define _DDDRVPORTRTOS_H_

//
// definitions copied from u-types.h

#ifndef __IAR_SYSTEMS_ICC__
#define __root
#endif

#ifndef __u1_defined
# define __u1_defined
typedef unsigned char u1;		///< boolean
//typedef int bool_t;		// initial definition from Oryx but should not be used
#endif

#ifndef __u8_defined
# define __u8_defined
#ifdef OPT_CHAR_IS_UNSIGNED
typedef char u8; 	//char is already forced as unsigned by compiler
					//avoid conflicts with string manipulations
#else
typedef unsigned char u8;		///< 8 bits unsigned
#endif
#endif

#ifndef __s8_defined
# define __s8_defined
typedef signed char s8;			///< 8 bits signed
#endif

#ifndef __u16_defined
# define __u16_defined
typedef unsigned short u16;		///< 16 bits unsigned
#endif

#ifndef __s16_defined
# define __s16_defined
typedef short s16;				///< 16 bits unsigned
#endif

#ifndef __u32_defined
# define __u32_defined
typedef unsigned int u32;		///< 32 bits unsigned
#endif

#ifndef __s32_defined
# define __s32_defined
typedef signed int s32;		///< 32 bits unsigned
#endif

typedef volatile u8  vu8;
typedef volatile u16 vu16;
typedef volatile u32 vu32;
typedef vu8  bf8;		//inner volatile to avoid byte access optimisation
typedef vu16 bf16;		//inner volatile to avoid byte access optimisation
typedef vu32 bf32;		//inner volatile to avoid byte access optimisation

typedef u1  T_BOOL;
typedef u8  T_BYTE;
typedef u16 T_WORD;
typedef u32 T_DWORD;
typedef unsigned long T_LONG; /* This type should not exists SINCE IT IS u32 or u64 according to the CPU */

typedef u32 T_ID;

#define MAX_signed32    ((s32)0x7FFFFFFF)
#define MIN_signed32    ((s32)0x80000000)

#define MAX_unsigned32  (0xFFFFFFFF)
#define MIN_unsigned32  (0)

#define MAX_signed16    ((s16)0x7FFF)
#define MIN_signed16    ((s16)0x8000)

#define MAX_unsigned16  (0xFFFF)
#define MIN_unsigned16  (0)

#define MAX_signed8     ((s8)0x7F)
#define MIN_signed8     ((s8)0x80)

#define MAX_unsigned8   (0xFF)
#define MIN_unsigned8   (0)

#define IS_BIT_SET(x,mask)              (((x) & (mask))==(mask))
#define IS_BIT_CLR(x,mask)              (((x) & (mask))==0)
#define BIT_SET(x,mask)                 ((x) |=  (mask))
#define BIT_CLR(x,mask)                 ((x) &= ~(mask))
#define BIT_MASK(x)                     (0x01 << x)

#define S16TOS32(x)                     ((x < 0)? (s32)((s16)(-1) * x) * -1: x)
#define S16TOFLOAT_DIV(x, m)            ((x < 0)? (float)((s16)(-1) * x) * -1 / (float)m: (float)x / (float)m)

#if defined (DDOS_FREE_RTOS)
#define __PACKED_STRUCT   __packed struct
#elif defined(DDOS_LINUX)
#define __PACKED_STRUCT   struct __attribute__ ((packed))
#else
#warning structures are not packed ! please define __PACKED_STRUCT
#define __PACKED_STRUCT   struct
#endif

#endif //_DDDRVPORTRTOS_H_