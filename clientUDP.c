// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ttyOverUDP.h"
#define MAXLINE 1024

void dump_frame(u8 *pbuffer, u8 len)
{
  char szMessage[MAXLINE] = {0};

  for (u8 i=0; (i < len) && (i < MAXLINE/2); i++)
  {
    sprintf(&szMessage[i*2], "%02X", pbuffer[i]);
  }

  printf("%s\n", szMessage);
}

// Driver code
int main() {
  char sndBuffer[MAXLINE]={0x0b, 0x00, 0x80, 0xc9, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xac};
  char rcvBuffer[MAXLINE]={0};
	int nbBytesToRead, nready;

  TtyOverUdpInitGen(0);

	int sockfd = TtyOverUdpOpen(0);


//Version avec attente active
/*  nbBytesToRead = MAXLINE;
  for (;;) {
    // new write operation
    if (nbBytesToRead > 1) {
      sleep(2);
      if (TtyOverUdpWriteDirectArray(0, sndBuffer, (u8)sndBuffer[0]) > 0) {
        printf("message sent.\n");
      }
    }

    nready = TtyOverUdpRxAvail(0);
    if (1 == nready) {
      nbBytesToRead = TtyOverUdpReadDirectArray(0, rcvBuffer, MAXLINE);
      printf("Message (length=%d)\n", nbBytesToRead);
      dump_frame(rcvBuffer, nbBytesToRead);
    }
  }
*/

  //version boucle sans attente active
  nbBytesToRead = MAXLINE;
	for (;;) {
    // new write operation
    if (nbBytesToRead > 1) {
      sleep(2);
      if (TtyOverUdpWriteDirectArray(0, sndBuffer, (u8)sndBuffer[0]) > 0) {
        printf("message sent.\n");
      }
    }

    usleep(10000);
    nbBytesToRead = TtyOverUdpReadDirectArray(0, rcvBuffer, 1);
    if (nbBytesToRead > 0) {
      nbBytesToRead = (u8)rcvBuffer[0];
      nbBytesToRead = TtyOverUdpReadDirectArray(0, rcvBuffer, (nbBytesToRead >= MAXLINE)?MAXLINE:nbBytesToRead);
      printf("Message (length=%d)\n", nbBytesToRead);
      dump_frame(rcvBuffer, nbBytesToRead);
    }
	}


	TtyOverUdpClose(0);
	return 0;
}
